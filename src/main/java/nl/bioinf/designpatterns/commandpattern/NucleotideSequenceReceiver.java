package nl.bioinf.designpatterns.commandpattern;

/**
 * Creation date: Jul 07, 2017
 *
 * @author Michiel Noback (&copy; 2017)
 * @version 0.01
 */
public class NucleotideSequenceReceiver {
    private static final NucleotideSequenceReceiver instance = new NucleotideSequenceReceiver();

    private StringBuilder sequence = new StringBuilder();

    public static NucleotideSequenceReceiver getInstance() {
        return instance;
    }

    public void append(char c) {
        this.sequence.append(c);
    }

    public int length() {
        return this.sequence.length();
    }

    public void clearAll() {
        this.sequence = new StringBuilder();
    }

    public void deleteLast() {
        sequence.deleteCharAt(sequence.length() - 1);
    }

    public void toggleCase() {
        if (this.sequence.length() > 0) {
            String temp = this.sequence.toString();
            if (Character.isUpperCase(this.sequence.charAt(0))) {
                temp = temp.toLowerCase();
            } else {
                temp = temp.toUpperCase();
            }
            this.sequence = new StringBuilder(temp);
        }
        //have call on empty sequence pass silently
    }


    @Override
    public String toString() {
        return "sequence=" + sequence;
    }
}
