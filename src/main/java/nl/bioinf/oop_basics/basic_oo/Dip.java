package nl.bioinf.oop_basics.basic_oo;

/**
 * Created by michiel on 16/03/2017.
 */
public enum Dip {
    MUSTARD,
    KETCHUP,
    CHILI,
    GUACEMOLE;
}
