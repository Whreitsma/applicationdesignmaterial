package nl.bioinf.refactoring.lesson_exercise;

public class OnlineOrderService extends AbstractPizzaOrderClient {

    public OnlineOrderService(Pizzeria pizzeria) {
        super(pizzeria);
    }

    public void getHacked() {
        super.pizzeria.orders = null;
    }
}
