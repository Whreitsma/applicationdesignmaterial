package nl.bioinf.refactoring.lesson_exercise;

import nl.bioinf.designpatterns.builder.Pizza;

import java.util.List;

public interface PizzaOrderClient {
    void takeOrder(Pizza pizza);

    void takeOrders(List<Pizza> pizzas);

    void cancelOrder(Pizza pizza);
}
